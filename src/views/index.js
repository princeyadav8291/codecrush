import React from 'react'

import { Image, Button } from 'react-bootstrap'

import Img1 from "../assets/images/hero.jpg"
import Img2 from "../assets/images/farmer1.jpg"
import Img3 from "../assets/images/img3.jpg"
import FeatureProduct from '../components/featureProduct'

import "../assets/css/home.scss"




const Index = () => {
    return (
        <>
            <div className='main-container'>
                <div className='crousel'>
                    <div id="myCarousel" class="carousel slide pointer-event" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-label="Slide 1" aria-current="true"></button>
                            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2" class=""></button>
                            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3" class=""></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item carousel-item-next carousel-item-start">
                                <Image src={Img2} alt="product-details" width="100%" height="50%" className="bd-placeholder-img" />

                                <div class="container">
                                    <div class="carousel-caption text-start">
                                        <h1>Example headline.</h1>
                                        <p>Some representative placeholder content for the first slide of the carousel.</p>
                                        <p><a class="btn btn-lg btn-primary" href="#">Sign up today</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <Image src={Img1} alt="product-details" width="100%" height="50%" className="bd-placeholder-img" />

                                <div class="container">
                                    <div class="carousel-caption">
                                        <h1>Another example headline.</h1>
                                        <p>Some representative placeholder content for the second slide of the carousel.</p>
                                        <p><a class="btn btn-lg btn-primary" href="#">Learn more</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item active carousel-item-start">
                                <Image src={Img3} alt="product-details" width="100%" height="50%" className="bd-placeholder-img" />
                                <div class="container">
                                    <div class="carousel-caption text-end">
                                        <h1>One more for good measure.</h1>
                                        <p>Some representative placeholder content for the third slide of this carousel.</p>
                                        <p><a class="btn btn-lg btn-primary" href="#">Browse gallery</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
                <div className='row'>
                    <FeatureProduct />
                </div>
            </div>

        </>)
}

export default Index