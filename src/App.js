import React from 'react'
import IndexRouters from './Router'

const App = () => {
  return (
    <>
    <IndexRouters />
    </>
  )
}

export default App