import React from 'react'
import Footer from '../components/footer'
import Navbar from '../components/navbar'
import DefaultRouter from '../Router/default'

const Default = () => {
  return (
    <>
    <Navbar />
    <div className='container'>
        <DefaultRouter/>
    </div>
    <Footer />
    

    </>
  )
}

export default Default