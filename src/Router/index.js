import React from 'react'


//router 
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


//layoutpages
import Default from '../layout/defaultlay'


const IndexRouters = () => {
    return (
        <>
        <Router>
        <Switch>
          <Route  path="/" component={Default} />
       </Switch>
       </Router>
        </>
    )
}

export default IndexRouters
