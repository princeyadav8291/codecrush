import React from 'react'
import Index from '../views/index'
import About from '../views/about';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


const DefaultRouter = () => {
    return (
            <>   
      <Router>
      <Switch>
        <Route  path="/" component={Index} />
        <Route  path="/about" component={About} />
     </Switch>
     </Router>
     </>
          
    )
}

export default DefaultRouter
